﻿#pragma once

namespace Announcements
{
	const FString NewMatchStartsIn(TEXT("New match starts in:"));
	const FString ThereIsNoWinner(TEXT("There is no winner :("));
	const FString MultipleWinners(TEXT("There are multiple winners\n"));
	const FString TeamsTie(TEXT("Teams tied for the win:\n"));
	const FString WinnerTeam(TEXT("Winner team:\n"));
	const FString RedTeam(TEXT("Red team"));
	const FString BlueTeam(TEXT("Blue Team"));
}

// Copyright Epic Games, Inc. All Rights Reserved.

#include "DeathmatchParty.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DeathmatchParty, "DeathmatchParty" );

﻿# Deathmatch Party

### Online mulitplayer game!

Deathmatch Party is an online multiplayer game that uses the Steam Services to connect 
players allowing them to play together.

Every player should find the weapons waiting across the map to eliminate other players.

![MainMenuPicture](./PortfolioPictures/MainMenu_Portfolio.png)

![LobbyPicture](./PortfolioPictures/Lobby_Portfolio.png)

![BigMap](./PortfolioPictures/BigMap_Portfolio.png)

![PlayingMatch](./PortfolioPictures/PlayingMatch_Portfolio.gif)